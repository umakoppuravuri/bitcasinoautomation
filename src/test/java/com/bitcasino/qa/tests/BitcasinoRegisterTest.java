package com.bitcasino.qa.tests;

import java.net.MalformedURLException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.bitcasino.qa.base.TestBase;
import com.bitcasino.qa.pages.BitcasinoLoginPage;
import com.bitcasino.qa.pages.BitcasinoRegisterPage;
import com.bitcasino.qa.util.TestUtil;

public class BitcasinoRegisterTest extends TestBase {

	BitcasinoRegisterPage registerPage;
	TestUtil testUti = new TestUtil();

	String sheetName = "register";

	public BitcasinoRegisterTest() {
		super();
	}

	@BeforeMethod
	public void setUp() throws MalformedURLException{
		init();
		registerPage = new BitcasinoRegisterPage();	
	}

	@DataProvider
	public Object[][] getBitcasinoTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	@Test(priority=0,dataProvider="getBitcasinoTestData")
	public void Bitcasino_RegisterTest (String username, String password, String email, String day, String month, String year) throws InterruptedException{
		registerPage.signUpnow_Bitcasino(username, password, email, day, month, year);

	}

	@AfterMethod
	public void tearDown(){
		//driver.close();
	}
}
