package com.bitcasino.qa.tests;

import java.net.MalformedURLException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.bitcasino.qa.base.TestBase;
import com.bitcasino.qa.pages.BitcasinoGamesPage;
import com.bitcasino.qa.util.TestUtil;

public class BitcasinoGamesTest extends TestBase {
	BitcasinoGamesPage gamesPage;
	TestUtil testUti = new TestUtil();

	public BitcasinoGamesTest() {
		super();
	}

	@BeforeMethod
	public void setUp() throws MalformedURLException{
		init();
		gamesPage = new BitcasinoGamesPage();	
	}


	@Test(priority=5)
	public void Bitcasino_GamesTest () {

		gamesPage.bitcasino_Searchgames();

	}

	@AfterMethod
	public void tearDown(){
		driver.close();
	}

}
