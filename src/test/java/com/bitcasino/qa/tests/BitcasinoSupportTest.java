package com.bitcasino.qa.tests;

import java.net.MalformedURLException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.bitcasino.qa.base.TestBase;
import com.bitcasino.qa.pages.BitcasinoRegisterPage;
import com.bitcasino.qa.pages.BitcasinoSupportPage;
import com.bitcasino.qa.util.TestUtil;

public class BitcasinoSupportTest extends TestBase{
	BitcasinoSupportPage supportPage;
	TestUtil testUti = new TestUtil();

	String sheetName = "login";

	public BitcasinoSupportTest() {
		super();
	}

	@BeforeMethod
	public void setUp() throws MalformedURLException{
		init();
		supportPage = new BitcasinoSupportPage();	
	}
	
	@DataProvider
	public Object[][] getBitcasinoTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	@Test(priority=4,dataProvider="getBitcasinoTestData")
	public void Bitcasino_WithdrawAccount(String username, String password) throws InterruptedException{
		supportPage.Withdraw_Account(username, password);

	}

	@AfterMethod
	public void tearDown(){
		//driver.close();
	}
}
