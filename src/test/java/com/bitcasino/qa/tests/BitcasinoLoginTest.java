package com.bitcasino.qa.tests;

import java.net.MalformedURLException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.bitcasino.qa.base.TestBase;
import com.bitcasino.qa.pages.BitcasinoLoginPage;
import com.bitcasino.qa.util.TestUtil;

public class BitcasinoLoginTest extends TestBase {
	BitcasinoLoginPage loginPage;
	TestUtil testUti = new TestUtil();

	String sheetName = "login";

	public BitcasinoLoginTest(){
		super();
	}

	@BeforeMethod
	public void setUp() throws MalformedURLException{
		init();
		loginPage = new BitcasinoLoginPage();	
	}

	@DataProvider
	public Object[][] getBitcasinoTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	@Test(priority=1)
	public void Bitcasino_PageTitleVerify() throws InterruptedException{
		String title = loginPage.validateLoginPageTitle();
		Assert.assertEquals(title,"Bitcoin Casino - First Licensed Online Bitcoin Casino | Bitcasino.io");
		Thread.sleep(3000);
		System.out.println("Page Title is " +  title);
	}

	@Test(priority=2, dataProvider="getBitcasinoTestData")
	public void Bitcasino_LoginTest(String username, String password) throws InterruptedException{
		loginPage.login(username, password);

	}

	@AfterMethod
	public void tearDown(){
		//driver.close();
	}
}
