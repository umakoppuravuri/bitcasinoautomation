package com.bitcasino.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.bitcasino.qa.util.TestUtil;
import com.bitcasino.qa.util.WebEventListener;

public class TestBase {

	public static WebDriver driver;
	public static Properties prop;
	public static Actions action;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static String CONFIG_PROPERTIES_PATH = "src/main/java/com/bitcasino/qa/resources/config.properties";

	public TestBase() {
		try {
			prop = new Properties();
			InputStream stream = new FileInputStream(new File(CONFIG_PROPERTIES_PATH));
			prop.load(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void init() throws MalformedURLException {
		String browserName = prop.getProperty("browser");
		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", prop.getProperty("chromepath"));
			ChromeOptions options = new ChromeOptions(); 
			options.addArguments("disable-infobars"); 
			driver = new ChromeDriver(options);
		} else if (browserName.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", prop.getProperty("geckopath"));
			driver = new FirefoxDriver();
		}
		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.Page_Load_Timeout, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.Implicity_Wait, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
		action = new Actions(driver);
	}
}
