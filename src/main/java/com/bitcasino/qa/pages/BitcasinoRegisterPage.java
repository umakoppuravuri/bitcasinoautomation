package com.bitcasino.qa.pages;

import java.util.Scanner;
import org.apache.poi.hslf.util.SystemTimeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.bitcasino.qa.base.TestBase;

public class BitcasinoRegisterPage extends TestBase {

	// Page Factory - OR:
	@FindBy(xpath = "//button[@class='pt-button pt-intent-primary cms_button_open_modal']")
	WebElement SignupnowBtnEle;

	@FindBy(name = "username")
	WebElement usernameEle;

	@FindBy(name = "password")
	WebElement passwordEle;

	@FindBy(name = "email")
	WebElement emailEle;

	@FindBy(xpath ="//body[@class='pt-overlay-open']//div[@class='pt-portal']//div[@class='pt-overlay pt-overlay-open pt-overlay-scroll-container vertical-center']//span//div[@class='scrollock01 pt-overlay-content']//div[@class='scrollock01-inner']//div[@class='pt-dialog small']//div[@class='pt-dialog-body']//div//form//div[@class='form-material']//div[@class='row haslabel']//div//div[@class='pt-field-group']//div[1]//select[1]")
	WebElement DayselectEle;

	@FindBy(xpath ="//body[@class='pt-overlay-open']//div[@class='pt-portal']//div[@class='pt-overlay pt-overlay-open pt-overlay-scroll-container vertical-center']//span//div[@class='scrollock01 pt-overlay-content']//div[@class='scrollock01-inner']//div[@class='pt-dialog small']//div[@class='pt-dialog-body']//div//form//div[@class='form-material']//div[@class='row haslabel']//div//div[@class='pt-field-group']//div[2]//select[1]")
	WebElement MonthselectEle;

	@FindBy(xpath ="//body[@class='pt-overlay-open']//div[@class='pt-portal']//div[@class='pt-overlay pt-overlay-open pt-overlay-scroll-container vertical-center']//span//div[@class='scrollock01 pt-overlay-content']//div[@class='scrollock01-inner']//div[@class='pt-dialog small']//div[@class='pt-dialog-body']//div//form//div[@class='form-material']//div[@class='row haslabel']//div//div[@class='pt-field-group']//div[3]//select[1]")
	WebElement YearselectEle;

	@FindBy(xpath = "//span[contains(text(),'I accept the')]")
	WebElement acceptcheckboxEle;

	@FindBy(xpath = "//span[contains(text(),'I agree to receive offers and emails from Bitcasin')]")
	WebElement agreecheckboxEle;

	@FindBy(xpath = "//div[@class='recaptcha-checkbox-checkmark']")
	WebElement captcha;

	@FindBy(xpath = "//span[contains(text(),'This is required')]")
	WebElement captchaErr;

	@FindBy(xpath = "//button[@type='submit']")
	WebElement submitBtnEle;

	// Initializing the Page Objects:
	public BitcasinoRegisterPage() {
		PageFactory.initElements(driver, this);
	}

	//Actions:
	public void signUpnow_Bitcasino(String username, String password, String email, String day, String month, String year) throws InterruptedException {
		SignupnowBtnEle.click();
		usernameEle.sendKeys(username);
		passwordEle.sendKeys(password);
		emailEle.sendKeys(email);
		Select daySelect = new Select(DayselectEle);
		daySelect.selectByVisibleText(day);
		Select Monthselect = new Select(MonthselectEle);
		Monthselect.selectByVisibleText(month);
		Select Yearselect = new Select(YearselectEle);
		Yearselect.selectByVisibleText(year);
		acceptcheckboxEle.click();
		agreecheckboxEle.click();		
		Thread.sleep(3000);
		submitBtnEle.click();
		Scanner scanner = new Scanner(System.in);
		System.err.println("Solve capcha and type 1 to continue with the next step: ");
		scanner.nextInt();
		submitBtnEle.click();
	}

}
