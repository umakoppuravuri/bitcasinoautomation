package com.bitcasino.qa.pages;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.bitcasino.qa.base.TestBase;

public class BitcasinoLoginPage extends TestBase{

	// Page Factory - OR:
	@FindBy(xpath = "//span[contains(text(),'Log in')]")
	WebElement LoginBtnEle;

	@FindBy(xpath = "//input[@name='username']")
	WebElement usernameEle;

	@FindBy(xpath = "//input[@name='password']")
	WebElement passwordEle;

	@FindBy(xpath = "//button[@type='submit']")
	WebElement submitBtnEle;

	// Initializing the Page Objects:
	public BitcasinoLoginPage() {
		PageFactory.initElements(driver, this);
	}

	//Actions:
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}

	public void login(String username, String password) throws InterruptedException {
		LoginBtnEle.click();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		usernameEle.sendKeys(username);
		passwordEle.sendKeys(password);
		Thread.sleep(3000);
		submitBtnEle.click();
		Scanner scanner = new Scanner(System.in);
		System.err.println("Solve capcha and type 1 to continue with the next step: ");
		scanner.nextInt();
		submitBtnEle.click();
	}
}
