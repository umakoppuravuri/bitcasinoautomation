package com.bitcasino.qa.pages;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.bitcasino.qa.base.TestBase;

public class BitcasinoSupportPage extends TestBase {
	BitcasinoLoginPage login = new BitcasinoLoginPage();

	// Page Factory - OR:
	@FindBy(xpath = "//span[contains(text(),'Support')]")
	WebElement SupportLink;

	@FindBy(xpath = "//button[contains(text(),'Withdraw now')]")
	WebElement WithdrawBtnEle;

	// Initializing the Page Objects:
	public BitcasinoSupportPage() {
		PageFactory.initElements(driver, this);
	}

	//Actions:
	public void Withdraw_Account(String username, String password) throws InterruptedException {
		SupportLink.click();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		WithdrawBtnEle.click();
		login.usernameEle.sendKeys(username);
		login.passwordEle.sendKeys(password);
		login.submitBtnEle.click();
		Scanner scanner = new Scanner(System.in);
		System.err.println("Solve capcha and type 1 to continue with the next step: ");
		scanner.nextInt();
		login.submitBtnEle.click();

	}
}
