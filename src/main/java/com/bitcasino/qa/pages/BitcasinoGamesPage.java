package com.bitcasino.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.bitcasino.qa.base.TestBase;

public class BitcasinoGamesPage extends TestBase{

	// Page Factory - OR:
	@FindBy(xpath = "//a[@href='/casino']//span[contains(text(),'Games')]")
	WebElement GamesLink;

	@FindBy(xpath = "//input[@placeholder='Search games']")
	WebElement SearchgamesEle;

	// Initializing the Page Objects:
	public BitcasinoGamesPage() {
		PageFactory.initElements(driver, this);
	}

	//Actions:
	public void bitcasino_Searchgames() {
		GamesLink.click();
		SearchgamesEle.sendKeys("sidewinder");
	}

}
