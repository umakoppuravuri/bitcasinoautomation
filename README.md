# Bitcasino Application

Automate the below test cases

# Test cases

### Testcase_001: Automate account creation with manual captcha
1. Open https://bitcasino.io/
2. Click on sign up button
3. Enter user name, password, email, date of birth, accept terms and conditions, agree to receive offers 
4. Click on Create account button 
5. Solve the captcha manually and confirm the script with an integer so it continues with next steps
6. On confirmation with integer, next step gets executed - click s on create account button
Expected: 
For new user, account gets created
For existing users, it throws an error saying

### Testcase_002: Login with manual captcha
1. Open https://bitcasino.io/
2. Login using test credentials umakselenium/Bitcasino@123
3. Click on log in button
4. Solve the captcha manually and confirm the script with an integer so it continues with next steps
5. On confirmation with integer, next step gets executed - click s on log in button

### Testcase_003: Verify page title
1. Open https://bitcasino.io/
2. Verify the Home page title if its "Bitcoin Casino - First Licensed Online Bitcoin Casino | Bitcasino.io" or not

### Testcase_004: Search for a game
1. Open https://bitcasino.io/
2. Click on Games link
3. Search for "sidewinder" in search games input box

### Testcase_005: Withdraw bitcoins from account with manual captcha
1. Open https://bitcasino.io/
2. Click on Support link 
3. Click on Withdraw now button
4. Login using umakselenium/Bitcasino@123 
5. Click on log in button
6. Solve the captcha manually and confirm the script with an integer so it continues with next steps
7. On confirmation with integer, next step gets executed - click s on log in button

# Instructions

Clone the repo:

Git:
```
$ git clone https://umakoppuravuri@bitbucket.org/umakoppuravuri/bitcasinoautomation.git
```

Or download a ZIP of master branch [manually](https://bitbucket.org/umakoppuravuri/bitcasinoautomation/get/25f3109f2cfb.zip) and expand the contents on your system

# Prerequisites

1. Tools: JAVA, TestNG, mvn, Eclipse (optional unless you want to use an editor)

# Test execution (two methods)

## Method 1: Using Command prompt 

1. Open command prompt and navigate to the expanded project folder

2. Run the command
	```
	mvn clean install or mvn test
	```	
	(This step installs required dependencies, generates jar file and runs the test suite)
    
## Method 2: Using Eclipse and TestNG

1. Open Eclipse and navigate to the expanded project folder

2. Right click on project root folder and Run as "Maven clean"
	(This step installs required dependencies and builds the project)
	
3. navigate to the folder where Testng.xml resides "src\main\resources", right click on Testng.xml file and Run as "TestNG suite"
	(This step runs the test suite and generates report under test-output folder\Extent.html)
    
4. Open Extent.html file to analyze the test report 